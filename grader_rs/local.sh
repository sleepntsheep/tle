#!/bin/sh

export TLE_TASKS_PATH=../tasks
export TLE_DATA_PATH=../data
export TLE_GRADER_PORT=8081

RUST_LOG=trace cargo run

