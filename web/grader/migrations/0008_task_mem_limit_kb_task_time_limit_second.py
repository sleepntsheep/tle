# Generated by Django 4.1.6 on 2023-02-05 04:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('grader', '0007_submission_submitted_at'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='mem_limit_kb',
            field=models.IntegerField(default=32768),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='task',
            name='time_limit_second',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
    ]
