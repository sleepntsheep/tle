import models
import os

for task in os.listdir('tasks'):
    if not os.path.isdir(task):
        continue

    desc_path = task + '/desc/'
    desc_path += os.listdir(desc_path) 

    print(f'\nTask: {task}')
    print(f'\tDesc: {desc_path}')

