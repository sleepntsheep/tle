#include <stdio.h>
#include <stdlib.h>

char s[100];
int w, h;

int main(void)
{
    scanf("%s", s);  // P3
    scanf("%d%d", &w, &h);
    scanf("%s", s);  // 255

    printf("P3\n%d %d\n255\n", w, h);
    for (int i = 0; i < h; i++) 
    {
        for (int j = 0; j < w; j++)
        {
            int r, g, b;
            scanf("%d%d%d", &r, &g, &b);
            if (r == 255 && g == 255 && b == 0) printf("0 0 0 ");
            else printf("%d %d %d ", r, g, b);
        }
        printf("\n");
    }
}

