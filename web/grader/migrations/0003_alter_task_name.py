# Generated by Django 4.1.6 on 2023-02-04 03:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('grader', '0002_task_desc'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='name',
            field=models.CharField(max_length=128, unique=True),
        ),
    ]
