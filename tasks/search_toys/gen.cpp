#include <bits/stdc++.h>
using namespace std;

int main() {
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist(1,50000);
    int k = dist(rng);
    cout << k << endl;

    std::uniform_int_distribution<std::mt19937::result_type> dk(1,k);
    int q = dk(rng);
    cout << '\n' << q << '\n';

    std::uniform_int_distribution<std::mt19937::result_type> dist2(1,1000000000);

    set<int> s;
    for (int i = 0; i < k; i++) {
        int t = dist2(rng);
        if(s.count(t)) {
            i--;
            continue;
        }
        s.insert(t);
        cout << t << ' ';
    }

    for (int i = 0; i < q; i++) {
        int t = dist2(rng);
        cout << t << '\n';
    }

    return 0;
}

