#include <iostream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>

using namespace std;
long long dp[10005];

int main() {
    dp[1] = 1;
    dp[2] = 1;
    dp[3] = 2;
    int T;
    cin >> T;
    for (int i =4;i <10005;i++)
        dp[i]=(dp[i-1]+dp[i-2]+dp[i-3]) % (long long)(1000007);

    while (T--){
        int n;
        cin >> n;
        cout << dp[n] << ' ';
    }

    return 0;
}
