use std::process::{Command, Stdio, Output};
use std::fs::{self, File};
use std::io::{prelude::*, BufReader};
use std::cmp::max;
use log::{trace, warn};
use anyhow::Result;
use std::env;
use lazy_static::lazy_static;

lazy_static! {
    pub static ref TASK_PATH : String = env::var("TLE_TASKS_PATH").unwrap();
    pub static ref DATA_PATH : String = env::var("TLE_DATA_PATH").unwrap();
    pub static ref SUBMISSION_PATH : String = env::var("TLE_DATA_PATH").unwrap().to_string() + "/submissions";
}

pub mod task;
pub mod cmp;
use cmp::{token_cmp, i64_cmp, f64_cmp, line_cmp};

pub struct SubmissionResult {
    pub raw: String,
    pub score: u64,
    pub mem_used: u64,
    pub time_used: u64,
}

pub struct CaseResult {
    pub raw: String,
    pub status: Option<String>,
    pub mem_used: u64,
    pub time_used: u64,
    pub exitcode: i32,
}

fn compile(sub_id: u64, task_name: &str, code: &str, out_path: &str) -> Result<Output, std::io::Error> {
    let path = format!("{}/{}/{}.cxx", *SUBMISSION_PATH, sub_id, task_name);
    fs::write(&path, code)?;

    Command::new("g++")
            .stdin(Stdio::piped())
            .args(vec![path.as_str(), "-std=c++17", "-O2", "-DEVAL", "-o", out_path])
            .output()
}

fn box_init() -> anyhow::Result<String> {
    Ok(std::str::from_utf8(
        &Command::new("isolate").arg("--init").output()?.stdout)?.trim().to_owned() + "/box")
}

fn parse_stat_file(stat_path: &str) -> CaseResult {
    let mut result = CaseResult {
        raw: String::from(""),
        status: None,
        mem_used: 0,
        time_used: 0,
        exitcode: 0,
    };

    let file = File::open(stat_path).unwrap();
    for line in BufReader::new(&file).lines() {
        let line = line.unwrap();
        let kv: Vec<&str> = line.split(":").collect();
        if kv.len() == 2 {
            match kv[0] {
                "status" => {
                    result.status = Some(kv[1].to_string());
                },
                "time" => {
                    result.time_used = max(result.time_used,
                                                 (1000 as f64 * kv[1].parse::<f64>().unwrap()) as u64);
                },
                "max-rss" => {
                    result.mem_used = max(result.mem_used, kv[1].parse().unwrap());
                },
                "exitcode" => {
                    result.exitcode = kv[1].parse::<i32>().unwrap();
                },
                _ => ()
            }
        }
    }
    result
}

fn merge_case(sub_result: &mut SubmissionResult, case_result: &CaseResult) {
    sub_result.raw += &case_result.raw;
    if case_result.exitcode == 0 {
        sub_result.mem_used = max(sub_result.mem_used, case_result.mem_used);
        sub_result.time_used = max(sub_result.time_used, case_result.time_used);
    }
}

pub fn grade(sub_id: u64, task_name: &str, code: &str) -> anyhow::Result<SubmissionResult> {
    Command::new("isolate").arg("--cleanup").output()?;

    fs::create_dir(format!("{}/{}", *SUBMISSION_PATH, sub_id)).unwrap();

    let mut sub_result = SubmissionResult {
        raw: "".to_string(),
        score: 0,
        mem_used: 0,
        time_used: 0
    };

    let boxpath = box_init().unwrap();
    let output = compile(sub_id, task_name, code, &format!("{}/exec", boxpath))?;
    if !output.status.success() {
        sub_result.raw = "CompilationError".to_string();
        return Ok(sub_result);
    }
    trace!("Compiled {} successfully with exit code {:?}", sub_id, output.status.code());

    let task_config = task::get_task_config(task_name)?;

    dbg!(task_config.clone());

    let grade_case = |i: usize| -> anyhow::Result<CaseResult> {
        trace!("Grading {}th case of {}", i, sub_id);

        let in_path = format!("{}/{}/tests/{}.in", *TASK_PATH, task_name, i);
        let sol_path = format!("{}/{}/tests/{}.sol", *TASK_PATH, task_name, i);
        let sol = std::fs::read_to_string(sol_path)?;

        fs::copy(in_path, format!("{}/{}.in", boxpath, i))?;

        let stat_path = format!("{}/{}/{}.stat", *SUBMISSION_PATH, sub_id, i);

        Command::new("isolate").args([
            "--meta", stat_path.as_str(),
            "--mem", &task_config.mem.to_string(),
            "--time", &((task_config.time as f64 / 1000.0).to_string()),
            "--stdin", &format!("{}.in", i),
            "--stdout", &format!("{}.out", i),
            "--run", "--", "./exec" ]).output()?;

        fs::copy(format!("{}/{}.out", boxpath, i), format!("{}/{}/{}.out", *SUBMISSION_PATH, sub_id, i))?;

        let mut result = parse_stat_file(&stat_path);

        result.raw = String::from({
            if result.status == Some("TO".to_string()) || result.time_used > task_config.time {
                "T"
            } else if result.status == Some("XX".to_string()) {
                "A"
            } else if result.status.is_some() || result.mem_used > task_config.mem {
                "x"
            } else {
                let got = std::fs::read_to_string(format!("{}/{}.out", boxpath, i))?;
                let r = match task_config.cmp_type.as_str() {
                    "tkcmp" => token_cmp(&sol, &got),
                    "icmp" => i64_cmp(&sol, &got),
                    "fcmp" => f64_cmp(&sol, &got),
                    "linecmp" => line_cmp(&sol, &got),
                    &_ => {
                        warn!("Unknown cmp type for task {} : {}", task_name, task_config.cmp_type.as_str());
                        false
                    }
                };
                if r {
                    "P"
                }
                else {
                    "-"
                }
            }
        });

        Ok(result)
    };

    if let Some(subtasks) = task_config.subtasks {
        let mut start = 1usize;
        for (ind, subtask) in subtasks.iter().enumerate() {
            trace!("Grading {}th subtask of {}", ind, sub_id);

            let score = subtask[0];
            let ncase = subtask[1] as usize;
            let mut correct = true;

            sub_result.raw += "[";
            for i in 0usize..ncase {
                let case_result = grade_case(i + start)?;
                merge_case(&mut sub_result, &case_result);
                correct = correct && (case_result.raw == "P");
            }
            sub_result.raw += "]";

            if correct { sub_result.score += score; }

            start += ncase;
        }
    } else {
        for i in 1usize..=task_config.ncase {
            let case_result = grade_case(i)?;
            merge_case(&mut sub_result, &case_result);

            if case_result.raw == "P" {
                sub_result.score += (task_config.max_score as f64 / task_config.ncase as f64).ceil() as u64;
                if sub_result.score > task_config.max_score {
                    sub_result.score = task_config.max_score;
                }
            }
        }
    }

    Command::new("isolate").args(["isolate", "--cleanup"]).output()?;
    Ok(sub_result)
}


