#include <stdio.h>
#include <string.h>
#include <algorithm>

int k, b[21][21], c[21][21];
int dp[21][1<<21];

int O(int t, int m)
{
    if(m==((1<<k)-1)) return 0;
    int &d = dp[t][m];
    if (d != -1) return d;
    d = 1e9;
    int tcst = 0;
    for (int j=0;j<k;++j) if (!(m&(1<<j)))tcst+=c[j][t];
    for (int i=0;i<k;++i)
    {
        if (!(m&(1<<i)))
        {
            d = std::min(d, b[i][t]+tcst + O(t+1,m|(1<<i)));
        }
    }
    return d;
}

int main() {
    memset(dp, -1, sizeof dp);
    scanf("%d", &k);
    for (int i = 0; i < k; ++i) for( int j=0;j<k;++j) scanf("%d",&b[i][j]);
    for (int i = 0; i < k; ++i) for( int j=0;j<k;++j) scanf("%d",&c[i][j]);

    printf("%d\n",O(0,0));

    return 0;
}


