#include <iostream>
using namespace std;

int main() {
    int t;
    cin >> t;
    while (t--){
        long long a, b;
        cin >> a >> b;
        long long x = 0;
        for (int j = a;  j <= b; j++)
            x += j;
        cout << x << " \n";
    }
    return 0;
}

