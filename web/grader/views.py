from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage
from django.http import HttpResponse
from django.views.static import serve
from django.shortcuts import render, redirect
from django.template import loader
from django import forms
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
import codecs
import sys


import datetime

from .grade import grade
from .models import Task, Submission

from threading import Thread
import os

SUBMISSION_PATH = os.getenv('TLE_DATA_PATH') + '/submissions'
TASK_PATH = os.getenv('TLE_TASKS_PATH')

def index(request):
    tasks = Task.objects.all().order_by('-id')
    ts = []

    if request.user.is_authenticated:
        for task in tasks:
            submissions = Submission.objects.filter(author = request.user,
                                        task = task).order_by('-id')
            ts.append((task, submissions.first()))
    else:
        for task in tasks:
            ts.append((task, None))

    context = { 'task_list': ts }
    return render(request, 'index.html', context)

PER_PAGE = 10

def submission0(request, page = 1, username = None, taskname = None):
    msg = ''

    if username is not None:
        submissions = Submission.objects.filter(author = User.objects.get(username = username)).order_by('-id')
        msg = f'for user {username}'
    elif taskname is not None:
        submissions = Submission.objects.filter(task = Task.objects.get(name = taskname)).order_by('-id')
        msg = f'for task {taskname}'
    else:
        submissions = Submission.objects.all().order_by('-id')
    

    paginator = Paginator(submissions, PER_PAGE)
    try:
        page_o = paginator.page(page)
    except EmptyPage:
        return index(request)

    return render(request, 'submission.html', {
        'submission_list': page_o,
        'msg': msg,
        'page' : page
    })

def submission(request, page = 1):
    return submission0(request, page)

def submission_task(request, taskname, page = 1):
    return submission0(request, page, taskname = taskname)

def submission_user(request, username, page = 1):
    return submission0(request, page, username = username)

def desc(request, task_name):
    t = Task.objects.get(name = task_name)
    filepath = f'{TASK_PATH}/{task_name}/{t.desc}'
    return serve(request, os.path.basename(filepath), os.path.dirname(filepath))


def check_admin(user):
   return user.is_superuser

@user_passes_test(check_admin)
def submission_code(request, sub_id):
    code_file = None
    for f in os.listdir(f'{SUBMISSION_PATH}/{sub_id}'):
        if f.endswith('.cxx'):
            code_file = f'{sub_id}/{f}'
            break

    print(f'{os.path.basename(code_file)}, {os.path.dirname(code_file)}')

    if code_file is not None:
        return serve(request, code_file, SUBMISSION_PATH)

class SubmitForm(forms.Form):
    task_id = forms.CharField(label="Task ID", max_length=100)
    file = forms.FileField()

@login_required
def submit(request):
    if request.method == 'POST':
        form = SubmitForm(request.POST, request.FILES)
        if form.is_valid():
            data = form.cleaned_data
            print(data)

            task_id = int(data['task_id'])
            task = Task.objects.get(id = task_id)
            print(task)

            submission = Submission.objects.create(author = request.user,
                                    task = task,
                                    submitted_at = datetime.datetime.now())


            file = request.FILES['file']
            code = file.read().decode('utf-8')

            Thread(target = grade,
                   args = (task.name, code, submission.id, )).start()

            messages.success(request, (f'Submitted {task.name}'))
            return redirect('/')
    else:
        form = SubmitForm()

    return render(request, 'submit.html', {'form': form})

