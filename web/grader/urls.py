from django.urls import path

from . import views

app_name = 'grader'
urlpatterns = [
    path('', views.index, name='index'),
    path('<str:task_name>/desc', views.desc, name='desc'),
    path('submit/', views.submit, name='submit'),
    path('submission/', views.submission, name='submission'),
    path('submission/<int:page>/', views.submission, name='submission'),
    path('submission_code/<int:sub_id>/', views.submission_code, name='submission_code'),
    path('submission_code/<int:sub_id>/<int:page>/', views.submission_code, name='submission_code'),
    path('submission/user/<str:username>/', views.submission_user, name='submission_user'),
    path('submission/user/<str:username>/<int:page>/', views.submission_user, name='submission_user'),
    path('submission/task/<str:taskname>/', views.submission_task, name='submission_task'),
    path('submission/task/<str:taskname>/<int:page>/', views.submission_task, name='submission_task'),
]

