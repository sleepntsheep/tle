from django.conf import settings
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

class Command(BaseCommand):

    def handle(self, *args, **options):
        if User.objects.count() == 0:
            admin = User.objects.create_superuser(username='root', password='root')
            admin.is_active = True
            admin.is_admin = True
            admin.save()
            print(f'Created default root account (user = root, password = root)\n*Please change the password immediately')
        else:
            print('Admin accounts can only be initialized if no User exist')

