#include <iostream>
using namespace std;

#define N 1000005
long long dp[N];
    
int main() {
    dp[0] = 1;
    for (int i = 1; i < N; i++)
        dp[i] = (dp[i-1]*i) % 1000000007;
    int T;
    long long a;
    cin >> T;
    while (T--) {
        cin >> a;
				//cout << 1 << endl;
        cout << dp[a] << endl;
    }
    return 0;
}

