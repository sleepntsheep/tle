#!/usr/bin/python3
from django.core.management import call_command

import json
import os

BASE = os.getenv('TLE_TASKS_PATH')

tasks = []

i = 1
for task in os.listdir(BASE):
    if not os.path.isdir(os.path.join(BASE, task)):
        continue
    print(f'Loading {task}')
    data = json.loads(open(f'{BASE}/{task}/manifest.json').read())

    mani = { 'model': 'grader.task', 'fields': { 'desc': 'desc.pdf',
                                                         'name': task }}
    i = i + 1

    for k, v in data.items():
        if k == 'mem':
            mani['fields']['mem'] = int(v)
        if k == 'time':
            mani['fields']['time'] = int(v)
        if k == 'desc':
            mani['fields']['desc'] = v
        if k == 'pk':
            mani['pk'] = int(v)

    tasks.append(mani)

with open('tasks.json', 'w') as f:
    json.dump(tasks, f)

os.system('python manage.py loaddata tasks.json')
os.remove('tasks.json')

