#include <iostream>
#include <ctime>
#include <cstdlib>
#include <chrono>
#include <unistd.h>

using namespace std;
int main() {
    srand(time(0) & 0xFFFF | (getpid() << 16));
    usleep(1000000);
    int n, k;
    n = rand() % 1001;
    k = rand() % ( n + 1);

    cout << n << ' ' << k << endl;

    for (int i = 0; i < n;i++) {
        cout << ((rand() << 16) & (rand() << 16)) % 1000001 << ' ';
    }

    return 0;
}

