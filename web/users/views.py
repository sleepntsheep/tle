from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.static import serve
from django.shortcuts import render
from django.template import loader
import django.contrib.auth as auth
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib import messages

def login2(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = auth.authenticate(request, username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return redirect('/')
        else:
            messages.success(request, ("There was an error logging in"))
            return redirect('login')
    else:
        return render(request, 'login.html', {})

def login(request):
    if request.method == 'POST':
        form = AuthenticationForm(data = request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = auth.authenticate(username = username, password = password)
            auth.login(request, user)
            messages.success(request, ('Login successful'))
            return redirect('/')
        else:
            messages.error(request, ("There was an error logging in"))
    else:
        form = AuthenticationForm()

    return render(request, 'login.html', { 'form': form }) 

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = request.POST['username']
            password = request.POST['password1']
            user = auth.authenticate(username = username, password = password)
            auth.login(request, user)
            messages.success(request, ('Registered successfully'))
            return redirect('/')
    else:
        form = UserCreationForm()

    return render(request, 'register.html', { 'form': form }) 

def logout(request):
    auth.logout(request)
    messages.success(request, ('Logged out'))
    return redirect('login')

