#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cstdint>
using namespace std;

uint64_t getRandom(uint64_t const& min = 0, uint64_t const& max = 0)
{
    return (((uint64_t)(unsigned int)rand() << 32) + (uint64_t)(unsigned int)rand()) % (max - min) + min;
}

#define N 1000005
long long dp[N];
    
int main() {
    dp[0] = 1;
    for (int i = 1; i < N; i++)
        dp[i] = (dp[i-1]*i) % (long long)(1e9+7);
    srand(time(0));
    int T;
    long long a;
    T = rand() % 101;
    cout << T << endl;
    while (T--) {
        a = getRandom(0, N);
        cout << a << endl;
    }
    return 0;
}

