#!/bin/sh

set -xe

mkdir -p "$TLE_DATA_PATH"
mkdir -p "$TLE_TASK_PATH"

sh -c 'cd web && python manage.py migrate --noinput && \
        python manage.py initadmin && python fetch_task.py && python manage.py runserver 0.0.0.0:8080' &

RUST_BACKTRACE=1 RUST_LOG=trace /app/grader

