#include <bits/stdc++.h>
using namespace std;

const int N = 100005;

int main() {
    std::cin.tie(nullptr)->sync_with_stdio(false);
    int n, m;
    cin >> n >> m;
    int sx, sy, d[n+1][m+1];
    memset(d, 63, sizeof d);
    cin >> sx >> sy;
    d[sy][sx] = 0;
    vector<pair<int, int>> upd;
    upd.emplace_back(sy, sx);
    for (int i = 1, x, y; i < 1ll * n * m; ++i) {
        cin >> x >> y;
        int ans = d[y][x];
        for (auto [by, bx] : upd) ans = min(ans, abs(by - y) + abs(bx - x));
        cout << ans << '\n';
        d[y][x] = 0;
        upd.emplace_back(y, x);
        if (upd.size() > 330) {
            queue<tuple<int, int, int>> q;
            for (auto [y, x] : upd) q.emplace(y, x, 0);
            while (q.size()) {
                auto [y, x, c] = q.front(); q.pop();
                const int dx[] = {1,-1,0,0}, dy[] = {0,0,1,-1};
                for (int j = 0; j < 4; ++j) {
                    auto ny = y + dy[j], nx = x + dx[j];
                    if (ny < 1 || nx < 1 || nx > m || ny > n) continue;
                    if (c + 1 < d[ny][nx])
                        q.emplace(ny, nx, d[ny][nx] = c + 1);
                }
            }
            upd.clear();
        }
    }

    return 0;
}


