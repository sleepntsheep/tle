#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cstdint>
using namespace std;

uint64_t getRandom(uint64_t const& min = 0, uint64_t const& max = 0)
{
    return (((uint64_t)(unsigned int)rand() << 32) + (uint64_t)(unsigned int)rand()) % (max - min) + min;
}

int main() {
    srand(time(0));
    int T;
    long long a;
    long long b;
    T = rand() % 101;
    cout << T << endl;
    while (T--) {
        a = getRandom(0, 1000000);
        b = getRandom(0, 1000000);
        if (a > b) swap(a, b);
        cout << a << ' '<< b << '\n';
    }
    return 0;
}

