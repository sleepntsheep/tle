#include <iostream>
#include <set>

using namespace std;

set<int> s;
int main() {
    cin.tie(0);ios::sync_with_stdio(0);
    int k, q;
    cin >> k >> q;
    for (int i = 0, x; i < k; i++) {
        cin >> x;
        s.insert(x);
    }

    for (int i = 0, x; i < q; i++) {
        cin >> x;
        cout << i << ' ';
        if (s.count(x)) {
            cout <<"YES ";
            continue;
        }

        auto it = s.lower_bound(x);
        auto it1 =s.upper_bound(x);
        if (it1==s.begin()){
            cout << *it1 << ' ';
            continue;
        }

        if (it == s.end()) {
            cout << *prev(it1) << ' ';
            continue;
        }
        it1--;

        if (abs(*it1 - x) < abs(*it - x)) {
            cout << *it1 << ' ';
        } else if (abs(*it1-x)>abs(*it-x)) {
            cout << *it << ' ';
        } else {
            cout << min(*it, *it1) << ' ';
        }
    }

    return 0;
}

