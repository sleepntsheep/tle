
pub fn i64_cmp(a: &str, b: &str) -> bool {
    let aw: Vec<_> = a.split_whitespace().collect();
    let bw: Vec<_> = b.split_whitespace().collect();

    if aw.len() != bw.len()  {return false; }

    for (u, v) in bw.iter().zip(aw.iter()) {
        let ai: i64 = match u.parse() { Ok(v) => v, Err(_) => { return false } };
        let bi: i64 = match v.parse() { Ok(v) => v, Err(_) => { return false } };
        if ai != bi {
            return false;
        }
    }

    true
}

pub fn f64_cmp(a: &str, b: &str) -> bool {
    let aw: Vec<_> = a.split_whitespace().collect();
    let bw: Vec<_> = b.split_whitespace().collect();

    if aw.len() != bw.len()  {return false; }

    for (u, v) in bw.iter().zip(aw.iter()) {
        let ai: i64 = match u.parse() { Ok(v) => v, Err(_) => { return false } };
        let bi: i64 = match v.parse() { Ok(v) => v, Err(_) => { return false } };
        if ai != bi {
            return false;
        }
    }

    true
}


pub fn token_cmp(a: &str, b: &str) -> bool {
    let aw: Vec<_> = a.split_whitespace().collect();
    let bw: Vec<_> = b.split_whitespace().collect();

    if aw.len() != bw.len()  {return false; }

    for (u, v) in bw.iter().zip(aw.iter()) {
        if u != v {
            return false;
        }
    }

    true
}

pub fn line_cmp(a: &str, b: &str) -> bool {
    let aw: Vec<_> = a.lines().collect();
    let bw: Vec<_> = b.lines().collect();

    if aw.len() != bw.len()  {return false; }

    for (u, v) in bw.iter().zip(aw.iter()) {
        if u != v {
            return false;
        }
    }

    true
}

