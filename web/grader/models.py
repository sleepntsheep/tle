from django.db import models
from django.contrib.auth.models import User

class Task(models.Model):
    name = models.CharField(max_length = 128, unique = True)
    desc = models.CharField(max_length = 256)
    time = models.IntegerField()
    mem = models.IntegerField()

class Submission(models.Model):
    author = models.ForeignKey(User, on_delete=models.PROTECT)
    task = models.ForeignKey(Task, on_delete=models.PROTECT)
    status = models.CharField(max_length = 64, null = True)
    result = models.CharField(max_length = 64, null = True)
    score = models.IntegerField(null = True)
    submitted_at = models.DateTimeField()
    time_used_ms = models.IntegerField(null = True)
    mem_used_kb = models.IntegerField(null = True)

