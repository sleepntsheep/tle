#!/bin/sh

export TLE_DATA_PATH=../data
export TLE_TASKS_PATH=../tasks
export TLE_GRADER_PORT=8081

./fetch_task.py
python manage.py runserver 8080
