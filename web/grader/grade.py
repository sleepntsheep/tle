from .models import Submission, Task
from django.core.files.storage import default_storage
import os
import shlex
import subprocess
import math
import shutil
import subprocess
import socket
import struct

PORT = int(os.environ.get('TLE_GRADER_PORT', '8081'))

class TestResult:
    def __init__(self, result: str, mem_used_kb: int, time_used_ms: int, score: 0):
        self.mem_used_kb = mem_used_kb
        self.time_used_ms = time_used_ms
        self.result = result
        self.score = score

def send_string(s: str, sock):
    e = s.encode()
    sock.send(struct.pack("Q", len(e)))
    sock.send(e)

def grade(task_name: str, code: str, sid: int):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(('127.0.0.1', PORT))
    send_string(task_name, sock)
    sock.send(struct.pack("Q", sid))
    send_string(code, sock)

    data = sock.recv(1024).decode().rstrip('\x00').split()

    su = Submission.objects.filter(id = sid).update

    if (len(data) < 4):
        su(result = 'Error during grading')
    else:
        su(result = data[0])
        su(mem_used_kb = int(data[1]))
        su(time_used_ms = int(data[2]))
        su(score = int(data[3]))

