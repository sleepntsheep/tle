#include "../test.hpp"


int main(int argc, char **argv) {
    InitGen();
    assert(argv[1] && argv[2]);
    int n = std::stoi(argv[1]);
    int m = std::stoi(argv[2]);
    int board[n+1][m+1];
    memset(board, 0, sizeof board);
    int sx = randint(1, m), sy = randint(1, n);
    std::vector<std::pair<int, int>> white;
    for (int i = 1; i <= n; ++i) for (int j = 1; j <= m; ++j)
        if (!(sx == j && sy == i)) white.emplace_back(j, i);
    std::cout << n << ' ' << m << '\n';
    std::cout << sx << ' ' << sy << '\n';
    for (int i = 1; i < n * m; ++i) {
        int left = white.size();
        int to_remove = randint(0, left-1);
        swap(white[to_remove], white[left-1]);
        auto removed = white.back();
        white.pop_back();
        std::cout << removed.first << ' ' << removed.second << '\n';
    }
    return 0;
}

