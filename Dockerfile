FROM alpine:latest as build

COPY deploy.sh /app/deploy.sh
COPY web /app/web
COPY grader_rs /app/grader_rs

WORKDIR /app

RUN apk add rust cargo git libcap-dev asciidoc libcap build-base && \
    git clone https://github.com/ioi/isolate --depth=1 && cd isolate && make && make install && cd .. && rm -rf isolate && \
    cd grader_rs && \
    cargo build --release && \
    mv target/release/grader /app/grader && \
    cd /app && \
    rm -rf grader_rs

FROM python:3.10-alpine3.17

COPY --from=build --chmod=777 /usr/local/bin/isolate /usr/local/bin/isolate
COPY --from=build --chmod=777 /usr/local/etc/isolate /usr/local/etc/isolate
COPY --from=build --chmod=777 /app /app
WORKDIR /app

RUN apk add --update --no-cache g++ libcap && \
    pip3 install --no-cache-dir -r /app/web/requirements.txt

EXPOSE 8080
ENTRYPOINT /app/deploy.sh

