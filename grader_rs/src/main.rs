use std::io::{Read, Write};
use std::net::{TcpListener, TcpStream};
use byteorder::{ByteOrder, LittleEndian};
use log::{trace, warn};
use anyhow::Result;
use std::fs::create_dir_all;
use std::env;

use crate::grader::{DATA_PATH, SUBMISSION_PATH};
mod grader;
use grader::grade;

fn recv_u64(mut s: &TcpStream) -> Option<u64> {
    let mut buf = [0u8; 8];
    if let Err(_) = s.read(&mut buf) { return None; }
    Some(LittleEndian::read_u64(&buf))
}

fn recv_string(mut s: &TcpStream) -> Option<String> {
    let length = recv_u64(s)?;
    let mut text = vec![0u8; length as usize];
    if let Err(_) = s.read(&mut text) { return None; }

    match std::str::from_utf8(&text) {
        Ok(v) => Some(v.to_string()),
        Err(_) => None,
    }
}

fn main() -> Result<()> {
    env_logger::init();
    create_dir_all(&*SUBMISSION_PATH)?;

    let _ = &*DATA_PATH;

    let port = env::var("TLE_GRADER_PORT").unwrap().parse::<u16>().unwrap();
    let ss = TcpListener::bind(("127.0.0.1", port))?;

    for s in ss.incoming() {
        if let Ok(mut s) = s {
            let task = match recv_string(&s) { Some(v) => v, None => continue };
            let sub_id = match recv_u64(&s) { Some(v) => v, None => continue };
            let code = match recv_string(&s) { Some(v) => v, None => continue };

            trace!("Grading task {task}, id {sub_id}");

            let result = grade(sub_id, &task, &code)?;
            s.write(format!("{} {} {} {}", result.raw,
                            result.mem_used, result.time_used, result.score).as_bytes())?;
        }
    }

    Ok(())
}

