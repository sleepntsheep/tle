use std::fs::File;
use crate::grader::TASK_PATH;
use anyhow::Result;
use serde::{Deserialize, Serialize};

const fn mem_limit_default() -> u64 {
    256000
}

const fn time_limit_default() -> u64 {
    1000
}

const fn max_score_default() -> u64 {
    1000
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct TaskConfig {
    #[serde(default = "mem_limit_default")]
    pub mem: u64,
    #[serde(default = "time_limit_default")]
    pub time: u64,
    pub ncase: usize,
    #[serde(default = "max_score_default")]
    pub max_score: u64,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub subtasks: Option<Vec<Vec<u64>>>,
    #[serde(default)]
    pub cmp_type: String,
}

pub fn get_task_config(task_name: &str) -> Result<TaskConfig> {
    let file = File::open(format!("{}/{}/manifest.json", *TASK_PATH, task_name))?;
    let u = serde_json::from_reader(file)?;
    Ok(u)
}

