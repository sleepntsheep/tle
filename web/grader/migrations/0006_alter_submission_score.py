# Generated by Django 4.1.6 on 2023-02-04 09:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('grader', '0005_submission_score'),
    ]

    operations = [
        migrations.AlterField(
            model_name='submission',
            name='score',
            field=models.IntegerField(null=True),
        ),
    ]
